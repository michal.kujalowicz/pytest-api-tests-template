# Pytest REST API Template

## Prerequisites

- Python 3.7+
- PyCharm
- Terminal

## Windows

- Open terminal (e.g. [cmder])
- Prepare virtual environment:

	venv-install.bat

- Install dependencies:

	pip install -r requirements.txt
	
- Run all tests:

    pytest tests


```
== test session starts ==

collected 1 item

tests/test_hello_rest_api.py .  [100%]

== 1 passed in 5.12s ==
```

- Import project to PyCharm

## macOS

- Open terminal
- Prepare virtual environment

	python -m venv venv
	source venv/bin/activate
	

- Install dependencies:

	pip install -r requirements.txt

- Run all tests:

    pytest tests
    
- Import project to PyCharm
